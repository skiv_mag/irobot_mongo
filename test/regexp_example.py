# 63475.08

# 764,989 465.8

# 4675,384.847


# (number '(\d+[ |,])*\d+\.\d+')
# (date, '(\d{2}/\d{2}/\d{2})+( \d{2}:\d{2}:\d{2})?')


dict_r = [
{'regexp': u'-?(\\d+[ |,])*\\d+\\.\\d+', 'type': u'number', 'value': u'next_total'}, 
{'regexp': u'-?(\\d+[ |,])*\\d+\\.\\d+', 'type': u'number', 'value': u'payment_trans'}, 
{'regexp': u'-?(\\d+[ |,])*\\d+\\.\\d+', 'type': u'number', 'value': u'regular_trans'}, 
{'regexp': u'(\\d{2}/\\d{2}/\\d{2,4})+( \\d{2}:\\d{2}:\\d{2})?', 'type': u'date', 'value': u'next_slika_date'}
]

reg_dict = filter(lambda x: x['value']=="next_total" , dict_r)