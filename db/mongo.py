#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-

from pymongo import Connection
from bson.objectid import ObjectId


connection = Connection('localhost', 27017)
connection.drop_database('irobot')
db = connection.irobot

regex = db.regexp_map.insert([
	{
	'type'		: 'number',
	'regexp'	: '-?(\d+[ ,])*\d+\.\d+'
	},
	{
	'type'		: 'date',
	'regexp'		: '(\d{2}\/\d{2}\/\d{2,4})+( \d{2}:\d{2}:\d{2})?'
	}
])

user_id = db.user.insert([
{
	"email" : 'test2@mail.com',
	'user_info': [
		{
			'solek_id':50,
			'card_id':'asdasdasd',
			'login':'asdasdad',
			'pswd':'werwrwer'
		},
		{
			'solek_id':51,
			'card_id':'asdasdasd',
			'login':'asdasdsad',
			'pswd':'1234562344'
		}
	]
}
])

db.rule.insert([
{
	'solek_id': 	50,
	'name_rule':	'isracard',
	'url':			'https://service.isracard.co.il/S_logon.jsp',
	'path': [
				{"order" : 1, "action":"put_id",		 "value" : "//*[@id='supplierId']"},
				{"order" : 2, "action":"put_login",	 "value" : "//*[@id='userCode']"},
				{"order" : 3, "action":"put_pswd",	 "value" : "//*[@id='password']"},
				{"order" : 4, "action":"click",		 "value" : "//*[@id='continue']"},
				{"order" : 5, "action":"click",		 "value" : "/html/body/form/div/header/div/div/div/a[2]"},
				{"order" : 6, "action":"click",		 "value" : "/html/body/form/table/tbody/tr[5]/td/table/tbody/tr/td/table/tbody/tr/td/ul/li[3]/ul/li"},
				{"order" : 7, "action":"get",		 "value" : ".//*[@id='futureCreditDetails118']/tbody"}
			],
	'user_uids': user_id,
	'parser': {
		"parser_type": 'html',
		"row_slice": '//tr[position()>4]',
		"values": {
			"next_slika_date": {"value": 'td[1]', 'type':regex[1]},
			"regular_trans":{"value": 'td[3]', 'type':regex[0]},
			"payment_trans":{"value": 'td[4]', 'type':regex[0]},
			"next_total": {"value": 'td[6]', 'type':regex[0]}
		}
	}

},
{
	'solek_id': 	51,
	'name_rule':			'tzameret',
	'url':			'https://service.tzm.co.il/LogIn.aspx',
	'path': [
				{"order" : 1, "action" : "put_id",	"value" : "//*[@id='txtUserName']"},
				{"order" : 2, "action" : "put_pswd", "value" : "//*[@id='txtPassword']"},
				{"order" : 3, "action" : "put_login", "value" : "//*[@id='txtUserCode']"},
				{"order" : 4, "action" : "click", "value" : "/html/body/form/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[6]/td/a"},
				{"order" : 5, "action" : "go", 	"value" : "https://service.tzm.co.il/TableTemplate.aspx?FutureCredit=true"},
				{"order" : 6, "action" : "get_js", "value" : "hDtSave"}
	],
	'user_uids': user_id,
	'parser': {
		"parser_type": "pipe",
		"row_slice": '[2, -1]',
		"values": {
			"next_slika_date": {"value": 0, "type": regex[1]},
			"regular_trans": {"value": None},
			"payment_trans": {"value": None},
			"next_total": {"value": 4, "type": regex[0]},
		}
	}

}
])




