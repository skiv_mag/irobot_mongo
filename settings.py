#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os.path

# work

root_path = os.path.dirname(__file__)
phantomjs_path = os.path.join(root_path, 'phantomjs/phantomjs_32')
db_path = os.path.join(root_path, 'db')
json_path = os.path.join(root_path, 'test/test.json')


# mongo parameters
mongo = {
    "host_mongo": "localhost",
    "port_mongo": 27017,
    "database_mongo": "irobot"
}

# debug_parameters
debug_mode = False

# json parameters

json_write_param = {"ensure_ascii": False}
#With this option size of json-file increases *3. Be carefully
pretty_json = True

if pretty_json:
    json_write_param["indent"] = 4
