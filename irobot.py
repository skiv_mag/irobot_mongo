#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-

# standart libraries
import io
import os
from time import localtime, strftime, strptime, time
from StringIO import StringIO
import json
import re
import traceback

# xml/html library
import lxml.etree as etree
from lxml.html.clean import clean_html

# mongo libraries
from pymongo import Connection
from bson.objectid import ObjectId

# selenium
from selenium import webdriver
from selenium.webdriver.common import proxy, desired_capabilities

import settings


def time_elapsed(func):
    # decorator for counting time
    def wrapper(*arg1):
        print 'Start of %s' % func.__name__
        start = time()
        res = func(*arg1)
        print "Elapsed Time: %s" % (time() - start)
        return res
    return wrapper


class DBProvider(Connection):

    def __init__(self, host_mongo, port_mongo, database_mongo):
        super(Connection, self).__init__(host_mongo, port_mongo)
        self.db = self[database_mongo]

    def get_settings(self, **kwargs):
        return list(self.db.rule.find())

    def get_userinfo(self, user_id, solek_id, **kwargs):
        return self.db.user.find_one({"_id": ObjectId(user_id)}, {'user_info': {'$elemMatch': {"solek_id": solek_id}}})

    def get_parser_settings(self, solek_id, **kwargs):
        return self.db.rule.find_one({"solek_id": solek_id}, {"parser": 1})

    def get_regexp_settings(self, **kwargs):
        return list(self.db.regexp_map.find())


# maybe webdriver.PhantomJS after fix some bugs(it could be faster)
class ActionDriver (webdriver.Firefox):

    def click_ext(self, path):
        return self.find_element_by_xpath(path).click()

    def put_ext(self, path, text):
        return self.find_element_by_xpath(path).send_keys(text)

    def submit_ext(self, path):
        return self.find_element_by_xpath(path).submit()


@time_elapsed
def process_scenario(driver, db):
    final_list = []

    for row in db.get_settings():
        for user_uid in row["user_uids"]:
            users_db = db.get_userinfo(user_uid, row["solek_id"])
            user = users_db["user_info"][0]
            user["id"] = str(users_db["_id"])
            try:
                driver.get(row["url"])
                try:
                    action_list = sorted(row['path'], key=lambda k: k['order'])
                except Exception, e:
                    print 'Not valid json format', e

                try:
                    for action in action_list:
                        act_type, path = action["action"], action["value"]
                        if act_type == 'put_login':
                            driver.put_ext(path, user["login"])
                        elif act_type == 'put_pswd':
                            driver.put_ext(path, user["pswd"])
                        elif act_type == 'put_id':
                            driver.put_ext(path, user["card_id"])
                        elif act_type == 'click':
                            driver.click_ext(path)
                        elif act_type == 'submit':
                            driver.submit_ext(path)
                        elif act_type == 'go':
                            driver.get(path)
                        elif act_type == 'get_js':
                            final_list.append(
                                (
                                    driver.execute_script("return document.getElementById('%s');" % path).get_attribute(
                                        "value").encode("utf-8"),
                                    'pipe',
                                    user["login"],
                                    user["solek_id"],
                                    user["id"]
                                )
                            )
                        elif act_type == 'get':
                            final_list.append(
                                (
                                    driver.find_element_by_xpath(
                                        path).get_attribute(
                                            "innerHTML").encode("utf-8"),
                                    'html',
                                    user["login"],
                                    user["solek_id"],
                                    user["id"]
                                )
                            )
                        else:
                            print 'Unknown action %s' % act_type
                            return
                        print "Action %s done" % act_type
                except Exception, e:
                    print "Action %s failed" % act_type
                    if settings.debug_mode:
                        traceback.print_exc()
                    else:
                        print e
                    print 'Row for card %s NOT processed!' % user["card_id"]
                    continue
                print "Row for card %s processed successfully" % user["card_id"]
            except Exception, e:
                if settings.debug_mode:
                    traceback.print_exc()
                else:
                    print e
                return
    return final_list


def clean_value(value, regexp_dict):
    match = re.search(regexp_dict['regexp'], value, re.MULTILINE)
    if match is not None:
        if regexp_dict['type'] == 'number':
            return match.group(0).translate(None, ' ,')
        elif regexp_dict['type'] == 'date':
            date = match.group(0)
            if re.match('(\d{2}/\d{2}/\d{2})$', date):
                date = strftime(
                    "%d/%m/%Y %H:%M:%S", strptime(date, '%d/%m/%y'))
            return date


def html_table_parser(db, html, unit_id):

    parser = etree.HTMLParser()
    tree = etree.parse(StringIO(clean_html(html.decode("utf-8"))), parser)
    regexp_settings = db.get_regexp_settings()
    parser_db = db.get_parser_settings(unit_id)
    parser_val = parser_db["parser"]["values"]
    row_slice = parser_db["parser"]["row_slice"]  # must be xpath format

    row_list = []
    for x in tree.xpath(row_slice):  # Selects all table without header
        new_dict = {}
        for key, val in parser_val.iteritems():
            y = x.xpath(val["value"])
            if y:
                # clean element, if have any something after postprocessing -
                # add it to general list
                el = y[0].text
                if el:
                    regx_dict = filter(
                        lambda x: x["_id"] == val["type"], regexp_settings)[0]
                    new_dict[key] = clean_value(el, regx_dict)
        row_list.append(new_dict)
    return row_list


def pipe_table_parser(db, string, unit_id):

    row_list = []

    regexp_settings = db.get_regexp_settings()
    parser_db = db.get_parser_settings(unit_id)

    parser_val = parser_db["parser"]["values"]
    slice_from, slice_to = eval(parser_db["parser"]["row_slice"])

    # looks strange, but splits pipe-format to in by one string of code
    for row in map(lambda x: x.split(','), string.split('|')[slice_from:slice_to]):
        new_dict = {}
        for key, val in parser_val.iteritems():
            if val["value"] is not None and val.has_key("type"):
                regx_dict = filter(
                    lambda x: x['_id'] == val["type"], regexp_settings)[0]
                try:
                    index = int(val["value"])
                except Exception, e:
                    print "Not valid format of parser_value for %s" % key
                new_dict[key] = clean_value(row[index], regx_dict)
            else:
                new_dict[key] = None
        row_list.append(new_dict)
    return row_list


def make_final_json(db, html_list):
    date = strftime("%d/%m/%Y %H:%M:%S", localtime())
    data_list = []
    parsers = {
        'html': lambda data, db, unit_id: html_table_parser(data, db, unit_id),
        'pipe': lambda data, db, unit_id: pipe_table_parser(data, db, unit_id)
    }

    for data, type_data, login, unit_id, user_uid in html_list:
        parser = parsers[type_data]
        result_data = parser(db, data, unit_id)
        data_list.append(
            {"company_id": user_uid, "user_name": login, "solek_id": unit_id, "data": result_data})
    return {"date_created": date, "general_data": data_list}


def write_to_file(path, json_text, json_settings):
    with io.open(settings.json_path, "w", encoding='utf-8') as f_out:
        f_out.write(
            unicode(json.dumps(json_text, **settings.json_write_param)))


def main(capabilities=None, only_test=False):
    db = DBProvider(**settings.mongo)

    driver = ActionDriver(capabilities=capabilities)
    driver.implicitly_wait(5)
    final_list = process_scenario(driver, db)
    driver.quit()

    final_json = make_final_json(db, final_list)
    write_to_file(settings.json_write_param,
                  final_json, settings.json_write_param)


if __name__ == '__main__':

    # proxy_settings###
    proxy_ip = '82.166.68.110:8978'

    p = proxy.Proxy({
                    'proxyType': proxy.ProxyType().MANUAL,
                    'httpProxy': proxy_ip,
                    'sslProxy': proxy_ip,
                    'ftpProxy': proxy_ip
                    })

    capabilities = desired_capabilities.DesiredCapabilities().FIREFOX
    p.add_to_capabilities(capabilities)
    # proxy_settings###

    main(
        capabilities=capabilities
    )
