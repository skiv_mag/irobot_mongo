#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask
from flask.ext.superadmin import Admin, model, BaseView


try:
    from mongoengine import *
except ImportError:
    exit('You must have mongoengine installed. Install it with the command:\n\t$> easy_install mongoengine')



# Defining MongoEngine Documents
parser_type = (('html','html'), ('pipe', 'pipe'))

class RegexpMap(Document):
    type = StringField(max_length=50)
    regexp = StringField(max_length=50)
    ff = DynamicField()

    def __unicode__(self):
        return self.type

    meta = {"allow_inheritance" : False}


class UserSettings(EmbeddedDocument):
    solek_id = IntField()
    card_id = StringField(max_length=50)
    login = StringField(max_length=50)
    pswd = StringField(max_length=50)
    meta = {"allow_inheritance" : False}


class User(Document):
    user_info = ListField(EmbeddedDocumentField(UserSettings))
    email = EmailField(max_length=50)

    def __unicode__(self):
        return self.email

    meta = {"allow_inheritance" : False}


class TypeValue(EmbeddedDocument):
    value = StringField(max_length=50)
    type = ReferenceField(RegexpMap, dbref=False)

    meta = {"allow_inheritance" : False}


class ParserValues(EmbeddedDocument):
    next_slika_date = EmbeddedDocumentField(TypeValue)
    regular_trans = EmbeddedDocumentField(TypeValue)
    payment_trans = EmbeddedDocumentField(TypeValue)
    next_total = EmbeddedDocumentField(TypeValue)

    meta = {"allow_inheritance" : False}
        

class Parser(EmbeddedDocument):
    parser_type = StringField(max_length=10, choices = parser_type)
    row_slice = StringField(max_length=50)
    values = EmbeddedDocumentField(ParserValues)

    meta = {"allow_inheritance" : False}


class Path(EmbeddedDocument):
    order = IntField()
    action = StringField(max_length=50)
    value = StringField(max_length=100)

    meta = {"allow_inheritance" : False}


class Rule(Document):
    solek_id = IntField()
    name_rule = StringField(max_length=50)
    url = URLField()
    path = SortedListField(EmbeddedDocumentField(Path))
    parser = EmbeddedDocumentField(Parser)
    user_uids = ListField(ReferenceField(User, dbref=False))

    meta = {"allow_inheritance" : False}


class UserModel(model.ModelAdmin):
    list_display = ('email',)
    # only = ('username',)

class RuleModel(model.ModelAdmin):
    list_display = ('url', 'solek_id', 'name_rule')


if __name__ == '__main__':
    # Create application
    app = Flask(__name__)

    # Flask views
    @app.route('/')
    @requires_auth
    def index():
        return '<a href="/admin/">Click me to get to Admin!</a>'


    # Create dummy secret key so we can use sessions
    app.config['SECRET_KEY'] = '123456790'
    app.config['DATABASE'] = 'irobot'


    # Mongo settings
    mongodb_settings = {
        'db':'irobot',
        # 'username':None,
        # 'password':None,
        # 'host':None,
        # 'port':None
    }

    # Connect to mongodb
    connect(**mongodb_settings)


    # Create admin
    admin = Admin(app, 'Irobot Admin')

    
    # Register the models
    admin.register(RegexpMap)
    admin.register(User, UserModel)
    admin.register(Rule, RuleModel)


    # Start app
    app.run('0.0.0.0', 8000, debug = True)
